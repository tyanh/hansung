﻿var pageindex = 0;
var leftmenuindex = 0;
function addrow(table, numrow) {
    var leght = $("."+table+" th").length;
    for (var i = 0; i < numrow; i++) {
        var tr = "<tr>";
        for (var j = 0; j < leght; j++) {
            tr += "<td></td>";
        }
        tr += "</tr>";
        $(tr).appendTo($("." + table + " tbody"));
    }

}
function loadhtml(page,leftmenu) {

    leftmenuindex = leftmenu;
    $(".topmenu").load("headmenu.html"); 
    if (page == 3) {
        $(".left-sidebar-pro").load("submenupage3.html");
        pageindex = 3;
    }
    else if (page == 1) {
        $(".left-sidebar-pro").load("submenu.html");
        pageindex = 2;
    }
    else if (page == 2) {
        $(".left-sidebar-pro").load("submenupage1.html");
        pageindex = 1;
    }
    else if (page == 5) {
        $(".left-sidebar-pro").load("submenupage5.html");
        pageindex = 5;
    }
}

$(window).load(function () {
    $(".mai-top-nav li:nth-child(" + pageindex + ")").css("background", "#3c7ab7");
    $(".metismenu li:nth-child(" + leftmenuindex + ")").css("background", "#cdff3b");
});

function modifyrow(table) {
    var o = $("." + table).find(".tr-active");
    var ordernumber = $(o).find(".ordernumber");
    var strnumber = ordernumber.html().toString();
    var array = strnumber.split('-');
    var lastIt = array[array.length - 1];

    lastIt = parseFloat(lastIt) + 1;
    if (lastIt < 10)
        lastIt = "0" + lastIt;
    array[array.length-1] = lastIt;
    ordernumber.html(array.toString().replace(/,/g,"-"));
}
function popupaddrow(table, numrow) {
    var leght = $("." + table + " th").length;
    var d = new Date();
    var month = d.getMonth() + 1;
    var p = $(".new-line").length + 1;
    if (p < 10)
        p = "0" + p;
    if (month < 10)
        month = "0" + month;
    var date = d.getDate();
    if (date < 10)
        date = "0" + date;
    var lenip = $(".table-popup input[type=text]").length;

    var newcode = d.getFullYear().toString() +  month.toString() +  date.toString();
    for (var i = 0; i < numrow; i++) {
        var tr = "<tr class='new-line'>";
        for (var j = 0; j < leght; j++) {
            if (j == 0)
                tr += "<td class='ordernumber'>P" + newcode + "-" + p + "-" + p +"</td>";
            else {
                var index = parseFloat(j) - 1;
                var vl = $(".table-popup input[type=text]:eq(" + index + ")").val();
                tr += "<td>" + vl+"</td>";
            }
        }
        tr += "</tr>";
        $(tr).prependTo($("." + table + " tbody"));
    }
    selectrow(table);
}
function selectrow(table) {
    $("."+table+" tr").click(function () {
        $("." + table + " tr").removeClass("tr-active");
        $(this).addClass("tr-active");
    });
}
function clickchangerow(tableclick,tbchange) {
    $("." + tableclick + " tr").click(function () {
        var id = $(this).index() + 1;

        $("." + tableclick + " tr").removeClass("tr-active");
        $(this).addClass("tr-active");
        $("." + tbchange + " tr").removeClass("tr-active");
        $("." + tbchange + " tr:eq(" + id+")").addClass("tr-active");
        return false;
    });
}
function includeHTML(indexx) {
    var tmp = indexx;
    var z, i, elmnt, file, xhttp;
    /* Loop through a collection of all HTML elements: */
    z = document.getElementsByTagName("*");
    for (i = 0; i < z.length; i++) {
        elmnt = z[i];
        /*search for elements with a certain atrribute:*/
        file = elmnt.getAttribute("w3-include-html");
        if (file) {
            /* Make an HTTP request using the attribute value as the file name: */
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4) {
                    if (this.status == 200) {
                       
                        elmnt.innerHTML = this.responseText;
                      
                    }
                    if (this.status == 404) { elmnt.innerHTML = "Page not found."; }
                     /* Remove the attribute, and call this function once more: */
                    elmnt.removeAttribute("w3-include-html");
                    
                    includeHTML();
                    $(".metismenu li:eq(" + tmp + ")").addClass("li-menu-active");
                }
            }
            xhttp.open("GET", file, true);
            xhttp.send();
            /* Exit the function: */
          
            return;
        }
    }
   
    
}
function includeheadmenu(index) {
    var z, i, elmnt, file, xhttp;
    /* Loop through a collection of all HTML elements: */
    z = document.getElementsByTagName("*");
    for (i = 0; i < z.length; i++) {
        elmnt = z[i];
        /*search for elements with a certain atrribute:*/
        file = elmnt.getAttribute("w3-include-html-headmenu");
        if (file) {
            /* Make an HTTP request using the attribute value as the file name: */
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4) {
                    if (this.status == 200) { elmnt.innerHTML = this.responseText; }
                    if (this.status == 404) { elmnt.innerHTML = "Page not found."; }
                    /* Remove the attribute, and call this function once more: */
                    elmnt.removeAttribute("w3-include-html-headmenu");
                    includeHTML();
                }
            }
            xhttp.open("GET", file, true);
            xhttp.send();
            /* Exit the function: */
            return;
        }
    }
}

